# Zendesk hide pending reminders

## Purpose

A userscript for Zendesk tickets.

This script hides the automatic pending reminders resulting from [Expand Agent follow-up workflow to Global Support (#3958)](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3958) in all comments. \
This compacts the ticket, making it a bit more readable, and make you scroll less.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_pending_reminders/script.user.js

## Changelog

- 1.0.1
  - Detect new sender signature
- 1.0.0
  - Public release
