'use strict';

function scrapeSfdcOpportunity(url) {
  let info = {};
  info.url = url;
  let labelNodes = document.querySelectorAll('.detailList tr td.labelCol');
  info.opportunity_name = labelToValue('Opportunity Name', labelNodes);
  info.account_name = labelToValue('Account Name', labelNodes);
  info.type = labelToValue('Type', labelNodes);
  info.product_details = labelToValue('Product Details', labelNodes);
  info.opportunity_owner = labelToValue('Opportunity Owner', labelNodes);
  info.close_date = labelToValue('Close Date', labelNodes);
  info.stage = labelToValue('Stage', labelNodes);
  info.quote_start_date = labelToValue('Quote Start Date', labelNodes);
  info.subscription_end_date = labelToValue('Subscription End Date', labelNodes);
  info.opportunity_term = labelToValue('Opportunity Term', labelNodes);
  info.parent_opportunity = labelToValue('Parent Opportunity', labelNodes);
  info.sales_qualified_source = labelToValue('Sales Qualified Source', labelNodes);
  return pasteSfdcOpportunity(info);
}

function pasteSfdcOpportunity(info) {
  let text =
    `SFDC Opportunity information:\n` +
    `URL = ${info.url}\n` +
    `Opportunity Name = ${info.opportunity_name}\n` +
    `Account Name = ${info.account_name}\n` +
    `Type = ${info.type}\n` +
    `Product Details = ${info.product_details}\n` +
    `Close Date = ${info.close_date}\n` +
    `Stage = ${info.stage}\n` +
    `Quote Start Date = ${info.quote_start_date}\n` +
    `Subscription End Date = ${info.subscription_end_date}\n` +
    `Opportunity Term = ${info.opportunity_term}\n` +
    `Parent Opportunity = ${info.parent_opportunity}\n` +
    `Opportunity Owner = ${info.opportunity_owner}\n` +
    `Sales Qualified Source = ${info.sales_qualified_source}\n`
  ;
  return text;
}
