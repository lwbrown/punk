# Zendesk move Preview button

## Purpose

A userscript for Zendesk tickets.

This script moves the Preview button from the far right of the screen,
next to the `Public reply` and `Internal note` buttons, saving you some mouse meters. 

<img src="img/screenshot.png" height=400 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_move_preview_button/script.user.js

## Changelog

- 1.0 
  - Public release
