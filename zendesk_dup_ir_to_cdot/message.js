'use strict';

// https://github.com/sweetalert2/sweetalert2
function _toast(icon, message) {
    let timerInterval
    Swal.fire({
        icon: icon,
        title: message,
        showConfirmButton: false,
        timer: 1500,
        willClose: () => {clearInterval(timerInterval)},
    });
}

function toast_ok(message) {
    _toast('success', message);
}

function toast_error(message) {
    _toast('error', message);
}

function toast_confirm(message) {
    Swal.fire({
        icon: 'warning',
        title: message,
        showConfirmButton: true
    });
}