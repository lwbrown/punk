// ==UserScript==
// @name          Zendesk duplicate internal request info to CDot
// @version       2.4
// @author        Rene Verschoor
// @description   Zendesk: duplicate internal request info to CDot
// @match         https://gitlab.zendesk.com/*
// @match         https://customers.gitlab.com/admin/*
// @match         https://customers.staging.gitlab.com/admin/*
// @grant         GM_registerMenuCommand
// @grant         GM_setValue
// @grant         GM_getValue
// @grant         GM_deleteValue
// @require       https://cdn.jsdelivr.net/combine/npm/@violentmonkey/dom@2,npm/@violentmonkey/ui@0.7
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @require       https://cdn.jsdelivr.net/npm/sweetalert2@11
// @require       message.js
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_dup_ir_to_cdot
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

// https://github.com/violentmonkey/vm-shortcut
const HOTKEY_COPY = 'ctrl-i ctrl-c';
const HOTKEY_PASTE = 'ctrl-i ctrl-v';

const AUTHOR = 'GitLab Support End User';

(function () {

  function registerHotkey() {
    VM.shortcut.register(HOTKEY_COPY, () => {
      infoCopy();
    });
    VM.shortcut.register(HOTKEY_PASTE, () => {
      infoPaste();
    });
  }

  function registerMenu() {
    GM_registerMenuCommand('IR info - copy', infoCopy);
    GM_registerMenuCommand('IR info - paste', infoPaste);
  }

  async function infoCopy() {
    await GM_deleteValue('IR_Info');
    let info = {};
    let url = document.URL;
    if (url.match(/^https:\/\/gitlab.zendesk.com\/agent\/tickets\/\d+/)) {
      info = scrapeInfo(url);
      await GM_setValue('IR_Info', JSON.stringify(info));
      if (info.validInfo) {
        toast_ok('Info copied');
      }
    } else {
      toast_error('Can only copy info from a Zendesk ticket');
    }
  }

  function scrapeInfo(url) {
    let info = {};
    info.validInfo = false;
    const title = getTicketTitle();
    if (title === 'IR - SM - Extend an existing trial') {
      info = scrapeInfo_sm_extend_existing_trial(url);
    } else if (title === 'IR - SM - Extend an (almost) expired subscription') {
      info = scrapeInfo_sm_extend_expired_subscription(url);
    } else if (title === 'IR - SM - Wider Community License') {
      info = scrapeInfo_sm_wider_community_license(url);
    } else if (title === 'IR - SM - NFR license request') {
      info = scrapeInfo_sm_nfr_license_request(url);
    } else if (title === 'IR - SM - Hacker One Reporter License') {
      info = scrapeInfo_sm_hacker_one_license(url);
    } else if (title === 'IR - SM - Problems starting a new trial') {
      info = scrapeInfo_sm_start_new_trial(url);
    } else {
      toast_error('Ticket type not recognized');
    }
    // console.log(info);
    return info;
  }

  function getTicketTitle() {
    return document.querySelector('div[role=tab][data-selected=true] [data-test-id="header-tab-title"]').textContent;
  }

  function getFirstComment() {
    let ticket= getActiveTicket();
    const comments = ticket.querySelectorAll('[data-test-id="ticket-audit-comment"]');
    if (!comments?.length) {
      toast_error('Ticket has no comments');
      return null;
    }
    const comment = comments[comments.length - 1];
    const author = comment.querySelector('div.content .header .actor .name').textContent.trim();
    if (author !== AUTHOR) {
      toast_error(`Unexpected author: ${author}`);
      return null;
    }
    return comment;
  }

  function getActiveTicket() {
    let workspaces = document.querySelectorAll('.workspace');
    let activeWorkspace;
    for (let workspace of workspaces) {
      if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
        activeWorkspace = workspace;
        break;
      }
    }
    return activeWorkspace;
  }

  function commentToList(comment) {
    let list = {};
    const nodes = comment.querySelectorAll('.zd-comment ul li');
    const texts = [...nodes].map(node => node.textContent);
    texts.forEach(text => {
      const keyval = text.split(/:(.*)/s);
      list[keyval[0].trim()] = keyval[1].trim();
    });
    return list;
  }

  function getList() {
    const comment = getFirstComment();
    return commentToList(comment);
  }

  function scrapeInfo_sm_extend_existing_trial(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "User count",
      null,
      "Plan",
      "Desired expiration date");
    values.trial = 'Yes';
    values.notes = url;
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_extend_expired_subscription(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "Seats needed in temp license",
      "True-ups needed in temp license",
      "Plan for temp license",
      "Desired expiration date");
    values.trial = 'Yes';
    values.notes = url;
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_wider_community_license(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "User count",
      null,
      "Plan",
      "Expiration date");
    values.trial = 'Yes';
    values.notes = `${url}, Wider Community`;
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_nfr_license_request(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "User count",
      null,
      "Plan",
      "License end date");
    values.trial = 'Yes';
    values.notes = `${url}, NFR`;
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_hacker_one_license(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "Number of users",
      null,
      "Plan",
      "License duration");
    values.expiration = '';  // clear field again
    values.trial = 'Yes';
    values.notes = `${url}, HackerOne`;
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_start_new_trial(url) {
    const list = getList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "Trial seat count",
      null,
      "Trial plan",
      "Desired trial end date");
    values.trial = 'Yes';
    values.notes = url;
    values.validInfo = true;
    return values;
  }

  function scrapeTicket(list, name, email, company, userscount, trueups, plan, expiration) {
    let values = {};
    values.name = list[name];
    values.email = getEmail(list[email]);
    values.company = list[company];
    values.userscount = list[userscount];
    values.trueups = trueups ? list[trueups] : null;
    values.plan = list[plan];
    values.expiration = list[expiration];
    return values;
  }

  function getEmail(value) {
    // Contact's email: foo.bar@example.com (Zuora Search) (Portal Search)
    let [email, ...rest] = value.split(' ');
    return email;
  }

  async function infoPaste() {
    let url = document.URL;
    if (url.match(/^https:\/\/customers\.(staging\.)?gitlab\.com\/admin\/license\/new_license/)) {
      let info = JSON.parse(await GM_getValue('IR_Info', '{}'));
      infoFill(info);
      await GM_deleteValue('IR_Info');
    } else {
      toast_error('Can only duplicate to CustomersDot > New License');
    }
  }

  function infoFill(info) {
    setLicenseType('Legacy License');
    setField('license_name', info.name);
    setField('license_company', info.company);
    setField('license_email', info.email);
    setField('license_users_count', info.userscount);
    setTrueups(info.trueups);
    setPlan('license_plan_code', capitalize(info.plan));
    setCheckbox('license_trial', info.trial);
    setExpiresDate(info.expiration);
    setField('license_notes', info.notes);
  }

  function setField(field, value) {
    document.getElementById(field).value = value;
  }

  function setLicenseType(type) {
    document.querySelector('[data-input-for="license_license_type"] .input-group-btn').click();
    [...document.querySelectorAll('li')].filter(option => option.textContent === type)[0].click();
  }

  function setTrueups(value) {
    if (value > 0) {
      toast_confirm("The request includes true-ups! You'll have to fill in PUC yourself!")
      setField('license_trueup_quantity', value);
    }
  }

  function setPlan(field, plan) {
    document.querySelector('[data-input-for="license_plan_code"] .input-group-btn').click();
    [...document.querySelector('ul#ui-id-2').children].filter(option => option.textContent === plan)[0].click();
  }

  function setExpiresDate(date) {
    let calendar = document.querySelectorAll('#license_expires_at_field div div .input-group input');
    calendar[1].focus();
    calendar[1].value = date;
    calendar[1].blur();
    document.getElementById('license_notes').focus();
  }

  function setCheckbox(field, text) {
    let value = text === 'Yes';
    let checkbox = document.getElementById(field);
    if (checkbox.checked !== value) {
      checkbox.click();
    }
  }

  function capitalize(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  registerMenu();
  registerHotkey();
  // console.log('@@@ 03');  // canary

})();

