// ==UserScript==
// @name          ZenDesk: Hide HIGH PRIORITY REMINDER
// @version       1.0.0
// @author        Katrin Leinweber
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_prio_reminder
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_prio_reminder/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_prio_reminder/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_prio_reminder/script.user.js
// ==/UserScript==

'use strict';

const DEBUG = false;
const HEAD = "HIGH PRIORITY REMINDER";
const L1 = "This is a high priority ticket";
const Q2 = "GitLab is Highly Degraded";
const L3 = "If you are lowering the priority";
const L4 = "I have lowered the priority to _";
const L5 = "> GitLab is Highly Degraded";
const L6 = "Thank you,";

(function () {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
    rm_quote();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find reminder content
  function po() {
    let selector = '[id^=ember] > div.comment > div > p';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      let text = element.textContent
      if (text === HEAD || text.startsWith(L1) || text.startsWith(L3) || text.startsWith(L4) || text.startsWith(L5) || text == L6) {
        go(elements, index);
      }
    })
  }

  // Find user info list entries
  function rm_quote() {
    let selector = '[id^=ember] > div.comment > div > blockquote > p';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      if (element.textContent.startsWith(Q2)) {
        go(elements, index);
      }
    })
  }

  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.backgroundColor = 'hotpink';
    } else {
      elements[index].style.display = 'none';
    }
  }

})();
