// ==UserScript==
// @name          Zendesk unsupported version
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: unsupported GitLab version warning
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_unsupported_version
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const SUPPORTED_VERSIONS = ['13', '14', '15'];

(function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    pogo();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function pogo() {
    let selector = '.custom_field_43970347 input';
    let elements = document.querySelectorAll(selector);
    elements.forEach((element, index) => {
      if (isUnsupported(element.value)) {
        highlightVersionField(element);
      }
    })
  }

  function isUnsupported(version) {
    let major = version.split('.')[0];
    return major.length && !SUPPORTED_VERSIONS.includes(major);
  }

  function highlightVersionField(field) {
    field.style.boxShadow='0px 0px 10px 5px red';
  }

})();
