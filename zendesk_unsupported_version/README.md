# Zendesk unsupported version

## Purpose

A userscript for Zendesk tickets.

This script alerts you when the customer seems to be on an unsupported GitLab version.

<img src="img/screenshot.png" height=300 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_unsupported_version/script.user.js

## Configuration

There is no configuration.\
The list of supported versions is hardcoded in the source code. \
Once a year, when GitLab jumps to a new major version, the userscript needs to be adapted. \
Everyone who has the userscript installed will automatically receive the updated version.

## Changelog

- 1.0.0
  - Public release
