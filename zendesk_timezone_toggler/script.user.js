// ==UserScript==
// @name          Zendesk timezone toggler
// @version       1.0.0
// @author        Anton Smith
// @description   Zendesk: toggles the timezone on comment timestamps between the local agent timezone and UTC
// @match         https://gitlab.zendesk.com/agent/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_timezone_toggler
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_timezone_toggler/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_timezone_toggler/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_timezone_toggler/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/
// ==/UserScript==
'use strict';

(function() {

    // Strip the friendly timezone from the local timezone
    const stripFriendlyTimezone = true;

    // Selector for time elements
    const timeSelector = 'time.live.full:not(.added-utc-toggle)';

    // Custom CSS for inserted elements
    const addCSS = css => document.head.appendChild(document.createElement("style")).innerHTML = css;
    addCSS(".toggle-time { color: #68737D; margin: 0 0 0 5px; } .created_at > .toggle-time { display: inline-block; margin: 0; } .actor > .toggle-time { padding-top: 2px; }");

    // Set as true initially so it shows the local time on init
    let showingUTC = true;

    // Process any time elements and calculate local/UTC times
    function timeMutations() {
        let elements = document.querySelectorAll(timeSelector);
        elements.forEach((element, index) => {
            let date = new Date(element.getAttribute('datetime'));

            element.setAttribute('local-time', stripFriendlyTimezone ? date.toString().split('(')[0] : date.toString());
            element.setAttribute('utc-time', date.toUTCString());

            // Hide the time element since we create our own
            element.style.display = 'none';

            let button = addToggleElements(element);
            toggleTimezone(button);
        })
    }

    // Add new time elements for the toggle
    function addToggleElements(element) {
        let button = document.createElement('button');
        button.addEventListener('click', toggleTimeEventListener);
        button.classList.add('toggle-timezone-btn');
        button.innerHTML = '<img src="https://static.zdassets.com/classic/images/emojis/clock3.png" height="15px" width="15px" alt="clock3" title="Toggle timezone">';
        element.parentNode.insertBefore(button, element.nextSibling);

        // Use a separate span instead of the time element
        // This is to stop Zendesk glitching out
        let span = document.createElement('span');
        span.classList.add('toggle-time');
        element.parentNode.insertBefore(span, element.nextSibling);

        // Add this so the mutator observer doesn't pick it up anymore
        element.classList.add('added-utc-toggle');

        return button;
    }

    // Event listener for the toggle button
    function toggleTimeEventListener(event) {
        event.preventDefault();

        showingUTC = !showingUTC;

        let buttons = document.querySelectorAll('button.toggle-timezone-btn');

        buttons.forEach((button) => {
            toggleTimezone(button);
        });
    }

    // Toggle the timezone shown to the user
    function toggleTimezone(button) {
        let span = button.parentNode.querySelector('span.toggle-time');
        let time = button.parentNode.querySelector('time');

        if (showingUTC) {
            span.innerHTML = time.getAttribute('local-time');

        } else {
            span.innerHTML = time.getAttribute('utc-time');
        }
    }

    // Watch for any change on the Zendesk page
    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    const observer = new MutationObserver(function(mutations, observer) {
        timeMutations();
    });

    observer.observe(document.body, {
        childList: true,
        subtree: true
    });
})();
