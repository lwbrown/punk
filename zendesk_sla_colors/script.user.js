// ==UserScript==
// @name          Zendesk SLA colors
// @version       1.0.4
// @author        Rene Verschoor
// @description   Zendesk: SLA color scale
// @match         https://gitlab.zendesk.com/*
// @grant         GM_getValue
// @grant         GM_setValue
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_sla_colors
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_sla_colors/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_sla_colors/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_sla_colors/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;
let intervalTimer;

(function() {

  const slaMinuteColors = {
    0: 'Red',
    15: 'OrangeRed',
    30: 'DeepPink',
    45: 'DeepPink'
  }

  const slaHourColors = {
    1: 'MediumVioletRed',
    2: 'MediumOrchid',
    3: 'MediumOrchid',
    4: 'MediumPurple',
    5: 'MediumPurple',
    6: 'SteelBlue',
    7: 'SteelBlue',
    8: 'Teal',
    9: 'Teal',
    10: 'DarkGreen',
    11: 'DarkGreen',
    12: 'ForestGreen'
  }

  const slaDayColors = {
    1: 'LimeGreen'
  }

  const breachedAnimation = `
    @keyframes pulse { 
      0%, 100% { background-color: Fuchsia; } 
      50% { background-color: DarkViolet; } 
    }
    `;

  GM_addStyle(breachedAnimation);

  function debug() {
    clearInterval(intervalTimer);
    GM_addStyle('.square { width: 100px; height: 50px; }');
    document.body.innerHTML = '';
    let html = '';

    function addDebugEntries(title, colors) {
      html += `<h1>${title}</h1>`;
      for (const [key, value] of Object.entries(colors)) {
        html += `<div class="square" style="background-color: ${value};"><h1>${key}</h1></div>`;
      }
    }

    addDebugEntries('Minute', slaMinuteColors);
    addDebugEntries('Hour', slaHourColors);
    addDebugEntries('Day', slaDayColors);
    document.getElementsByTagName('body')[0].innerHTML += html;
  }

  if (DEBUG) {
    intervalTimer = setInterval(function(){ debug(); }, 5000);
  }

  const observeNode = document.querySelector('body');
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    po(mutations);
  });
  observer.observe(observeNode, { childList: true, subtree: true });

  function po(mutations) {
    const selector = '[data-test-id="ticket-table-cells-sla"] div';
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll(selector).forEach( element => {
        go(element);
      });
    })
  }

  function go(element) {
    const sla = element.textContent;
    const unit = sla.slice(-1);
    const value = (sla === 'Now' ? 0 : Number(sla.slice(0, -1)));

    if (value > 0) {
      switch (unit) {
        case 'd' :
          colorSla(element, slaDayColors[1]);
          break;
        case 'h' :
          if (value >= 18) {
            colorSla(element, slaDayColors[1]);
          } else {
            colorSla(element, slaHourColors[Math.min(value, 12)]);
          }
          break;
        case 'm' :
          if (value >= 60) {
            colorSla(element, slaHourColors[1]);
          } else {
            colorSla(element, slaMinuteColors[Math.floor(value / 15) * 15]);
          }
          break;
        default :
          colorSla(element, 'Black');
      }
    } else {
      breachedSla(element);
    }
  }

  function colorSla(element, color) {
    element.style.color = 'white';
    element.style.backgroundColor = color;
  }

  function breachedSla(element) {
    element.style.animation = "pulse 2s infinite";
  }

})();
