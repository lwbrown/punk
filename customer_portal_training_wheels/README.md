
## Customer Portal Training Wheels

This userscript (non-destructively) augments Customer pages on the [GitLab Customer Portal](https://customers.gitlab.com/admin/) with some additional useful information.

Key features:

- Resolves GitLab.com UIDs and provides a shortcut link to the associated user profile at the top of the Customer page
- Detects whether a customer has **Read-Only** access or has **Sign-in disabled** and adds handy indicators near the top of the Customer page.

### Installation

1. Ensure you have a Userscript manager installed (such as [Tampermonkey](https://www.tampermonkey.net/) or [Violentmonkey](https://violentmonkey.github.io/)).
1. Open the `cdot_training_wheels.user.js` file, and click the **View Raw** button.
   - This should present your userscript manager's **Installation** page
1. When the script first runs you may receive a **permissions** check, as the userscript performs HTTP queries on your behalf (I encourage you to click **Trust Domain** on this screen)

### Modified Pages

#### Customer Pages

There are three badges that are added:

- The **Read-Only** badge - displayed when the **Billable** flag is disabled on a Customer
- The **Locked** badge - displayed when the **Login Activated** flag is disabled on a Customer
- A **GitLab.com Username** badge - displayed when the Customer has linked a GitLab.com account to their Customer Portal account

![Screenshot showing the Read-Only and Locked badges](./screenshots/badgeDemo1.png "Showing the Read-Only and Locked badges"){height=300px} 

![Screenshot showing the resolved Username badge](./screenshots/badgeDemo2.png "showing the resolved Username badge"){height=300px}

### Troubleshooting

#### UIDs aren't resolving

Please ensure that you are logged into GitLab.com in the same browser - the userscript takes advantage of your open session with GitLab.com to communicate with the API for UID resolution.

#### How can I use this on the staging environment?

Modify the line:

> // @match        https://customers.gitlab.com/admin/*

to instead state:

// @match        https://customers.staging.gitlab.com/admin/*


#### My issue isn't covered

Please [create an issue!](https://gitlab.com/rverschoor/punk/-/issues/new).