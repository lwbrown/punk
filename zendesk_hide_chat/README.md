# Zendesk hide chat

## Purpose

Zendesk suddenly started showing a `Zendesk Chat` icon. \
Obliterate!

<img src="img/chat.png" height=200 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_chat/script.user.js

## Changelog

- 1.0
  - Initial release
